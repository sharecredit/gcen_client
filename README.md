# GcenClient

Gcen::API client. Just for internal usage

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'gcen_client', '~> 0.1.3', git: 'https://bitbucket.org/sharecredit/gcen_client'
```

And then execute:

    $ bundle

