# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'gcen_client/version'

Gem::Specification.new do |spec|
  spec.name          = "gcen_client"
  spec.version       = GcenClient::VERSION
  spec.authors       = ["v.artemyev"]
  spec.email         = ["vlad@meyvndigital.co.uk"]
  spec.summary       = 'GCEN API client'
  spec.description   = 'GCEN client for easily make requests'
  spec.homepage      = "https://bitbucket.org/sharecredit/gcen_client"
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.8"
  spec.add_development_dependency "rake", "~> 10.0"

  spec.add_dependency "faraday_middleware", "~> 0.11.0"
end
