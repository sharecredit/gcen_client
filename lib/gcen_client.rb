require "gcen_client/version"
require 'singleton'
require 'faraday'

module GcenClient
  class API
    include Singleton

    attr_reader :status
    attr_reader :body

    REQUESTS = {
        registration: 'AMLRegister',
        gcs_balance: 'GCSBalance',
        investment_deposit: 'InvestmentDeposit',
        card_payment: 'CardPayment',
        check_card_payment_response: 'CheckCardPaymentResponse',
        payment_return_from_gcs_balance: 'PaymentReturnFromGCSBalance',
        gcs_balance_transfer: 'GCSBalanceTransfer',
        get_fx_quote: 'GetFXQuote',
        confirm_quote_with_card_payment: 'ConfirmQuoteWithCardPayment',
        confirm_quote_with_bank_transfer: 'ConfirmQuoteWithBankTransfer',
        credit_balance: 'CreditGCSBalance',
        check_cdd: 'CheckCDD',
        upload_documents: 'UploadIdDocuments',
        gcs_balance_log: 'GCSBalanceLog',
        running_gcs_balance: 'RunningGCSBalance',
        investment_collection_entries: 'InvestmentCollectionEntries',
        get_deal_history: 'GetDealHistory',
        get_outgoing_payments: 'GetOutgoingPayments',
        get_incoming_payments: 'GetIncomingPayments'
    }

    REQUESTS.each_pair do |k, v|
      define_method(k) do |params, client_id = ''|
        multipart = (k.to_s == 'upload_documents')
        Rails.logger.info("Request -> \e[0;32m#{v}\e[0m \e[0;31m#{JSON.pretty_generate(params)}\e[0m")
        make_post(multipart) do |r|
          r.url [v, client_id].reject{|e| e.blank?}.join('/')
          r.body = multipart ? params : params.to_json
        end
      end
    end

    private

    def make_post(multipart = false)
      @connection = Faraday.new(url: 'https://gcen.i-dash.co.uk/Api/') do |c|
        c.headers['Authentication'] = Rails.application.secrets.gcen_token
        c.request :multipart if multipart
        c.adapter Faraday.default_adapter
        c.options[:timeout] = 20
        c.options[:open_timeout] = 20
      end
      @response = @connection.post do |req|
        yield req
        req.headers['Content-Type'] = 'application/json' unless multipart
      end
      @status = @response.status
      Rails.logger.info("Response -> \e[0;32mCode:#{@response.status}\e[0m \e[0;31m#{JSON.pretty_generate(JSON.parse(@response.body))}\e[0m")
      @body = JSON.parse(@response.body)
    rescue => exception
      Rollbar.error(exception)
      { Message: exception.message, Success: false }
    end
  end
end
